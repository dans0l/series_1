# Development of A.P.I. Management System in Django

## Quick Guide
Below are the steps on how to get the web app up and running

1.	Clone it:
```bash
    git clone https://bitbucket.org/dans0l/series_1.git
```
2.	Cd into the Django web app:
```bash
    cd series_1/djangorestapis
```
3.	Create a venv
```bash
    python3 -m venv venv
```
4.	Activate venv:
```bash
    Mac/Linux: source venv/bin/activate
    Windows: venv\Scripts\activate
```
5.	Install the requirements
```bash
    pip install -r requirements.txt
```
6.	Create DB
```bash
    python manage.py makemigrations
```
7.	Apply DB Changes
```bash
    python manage.py migrate
```
8.	Create a super user by typing the command
```bash
    python manage.py createsuperuser
```
    - Enter the username
    - Enter the email
    - Enter the password
    - Remember these superuser credentials, they will be need when logging into the Django admin interface
9.	Run the server:
    - python manage.py runserver
10.	Navigate to your [localhost](http://127.0.0.1:8000) site
11.	Follow the instructions on the home page to start using the site 
