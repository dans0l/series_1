# the built in djnago User model
from django.contrib.auth.models import User
# the built in django serializers method used to convert python database objects to JSON objects
from rest_framework import serializers

# The UserCreateSerializer will be responsible to retrieving POST data passed to it 
# and using that user data to create a new user in the database
class UserCreateSerializer(serializers.ModelSerializer):
    # create another password filed to be used for password confirmation
    password2 = serializers.CharField(style={'input_type':'password'},write_only=True)

    # the class will interact with the User table and will require the data sent via 
    # POST to contain the email, password and password fields
    class Meta:
        model = User
        fields = ['email','password','password2']
        extra_kwargs = {
            'password':{'write_only':True}
        }

    # override the save method, so as to validate the confirmed password first before creating the new user
    def save(self, **kwargs):
        password = self.validated_data['password']
        password2 = self.validated_data['password2']

        if password2 != password:
            raise serializers.ValidationError({'password':'Passwords must match'})

        user = User(email=self.validated_data['email'],
                                   username=self.validated_data['email'])

        user.set_password(password)

        user.save()

        return user