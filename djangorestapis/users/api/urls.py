from django.urls import path
from users.api.views import create_user
# The in-built django restframework method responsible for logging a user in and returning their authorization token
from rest_framework.authtoken.views import obtain_auth_token

app_name = 'users'

urlpatterns = [
    # the A.P.I. U.R.L. for creating or registering as a new user
    path('create', create_user, name='api-create-user'),
    # the A.P.I. U.R.L. for logging into the A.P.I. system
    path('login', obtain_auth_token, name='api-log-in'),
]