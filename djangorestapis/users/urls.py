from django.urls import path
from users.views import register, profile
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('register/', register, name='register'),
    path('profile/', profile, name='profile'),
    path('login/', auth_views.LoginView.as_view(template_name="users/login.html"), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name="users/logout.html"), name='logout'),
    # a path to a page where you enter your email to request for a password reset
    path('passwod-reset/', auth_views.PasswordResetView.
         as_view(template_name="users/password_reset.html"),
         name='password_reset'),
    # the link to the page where you will reset your password, this link will be emailed
    path('passwod-reset-confirm/<uidb64>/<token>', auth_views.PasswordResetConfirmView.
         as_view(template_name="users/password_reset_confirm.html"),
         name='password_reset_confirm'),
    # a page to inform the user that an email with the link above has been successfully sent
    path('passwod-reset/done/', auth_views.PasswordResetDoneView.
         as_view(template_name="users/password_reset_done.html"),
         name='password_reset_done'),
    # the page a user will be redirected to once they reset their email from the emailed link above
    path('passwod-reset-complete', auth_views.PasswordResetCompleteView.
         as_view(template_name="users/password_reset_complete.html"),
         name='password_reset_complete'),
]
