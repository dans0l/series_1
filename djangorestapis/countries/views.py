from django.shortcuts import render, redirect
from countries.models import (API, Subscription)
from django.contrib import messages


# Create your views here.

def home(request):
    return render(request, 'countries/home.html')

def list_api(request):
    return render(request, 'countries/api_list.html')

def subscribe_api(request):
    return render(request, 'countries/api_subscribe.html')

def subscribed_apis(request):
    return render(request, 'countries/api_subscribed.html')

def unsubscribe_apis(request):
    return render(request, 'countries/api_unsubscribed.html')

def continents_apis(request):   
    context = {
        "subscriptions":len(Subscription.objects.filter(user=request.user,api_id=2))
    }
    return render(request, 'countries/api_continents.html', context=context)

def countries_apis(request):   
    context = {
        "subscriptions":len(Subscription.objects.filter(user=request.user,api_id=1))
    }
    return render(request, 'countries/api_countries.html', context=context)

def cities_apis(request):   
    context = {
        "subscriptions":len(Subscription.objects.filter(user=request.user,api_id=3))
    }
    return render(request, 'countries/api_cities.html', context=context)

def unsubscribe(request, api_id):
    message = ''
    try:    
        api = API.objects.get(pk=api_id)    
        subscription = Subscription.objects.get(api=api, user=request.user)
    except Subscription.DoesNotExist:
        message = 'Failed to unsubscribe'
    except API.DoesNotExist:
        message = 'Failed to unsubscribe'

    # if the subscription exists, delete it and inform the user appropriately
    operation = subscription.delete()
    if operation:
        message = f'Unsubscribed successful from {api}'
    else:
        message = f'Failed to unsubscribe from {api}'
    
    if len(message) > 0:
        messages.success(request, message)
    return redirect('home')

def subscribe(request, api_id):
    message = ''
    try:
        api = API.objects.get(pk=api_id)
    except API.DoesNotExist:
        message = 'Failed to subscribe'

    # Pass the user and the requested API to the subscription model for subscription
    # If a user already subscribed or a new subscription has been made inform the user appropriately
    operation = Subscription.objects.get_or_create(api=api,user=request.user)
    
    if operation:
        message = f'{api} subscription successful'
    else:
        message = f'{api} subscription failed'
    
    if len(message) > 0:
        messages.success(request, message)
    return redirect('home')

