from django.urls import path
from countries.views import (home, list_api, subscribe_api, subscribed_apis, unsubscribe_apis, 
continents_apis, countries_apis, cities_apis, unsubscribe, subscribe)

urlpatterns = [
    path('', home, name='home'),
    path('list_api', list_api, name='list-api'),
    path('subscribe_api', subscribe_api, name='subscribe-api'),
    path('subscribed_apis', subscribed_apis, name='subscribed-api'),
    path('unsubscribe_apis', unsubscribe_apis, name='unsubscribe-api'),
    path('continents_apis', continents_apis, name='continents-api'),
    path('countries_apis', countries_apis, name='countries-api'),
    path('cities_apis', cities_apis, name='cities-api'),
    path('unsubscribe/<int:api_id>', unsubscribe, name='unsubscribe'),
    path('subscribe/<int:api_id>', subscribe, name='subscribe'),
]
