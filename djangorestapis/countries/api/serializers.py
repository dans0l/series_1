from rest_framework import serializers
from countries.models import Continent, Country, Cities, API, Subscription

# Serializer classes are responsible for converting python database objects 
# to JSON objects that can be returned back as a response

# The Continent Serializer class will inherit the ModelSerializer class from the serializers package
class ContinentSerializer(serializers.ModelSerializer):
    # The JSON object to returned will be labeled values, that will get its data from a class method called get_continent_values
    values = serializers.SerializerMethodField('get_continent_values')

    class Meta:
        # We will set the serializer class to get data from the Continent model
        model = Continent
        # We then set the class to return values variable as the desired response
        fields = ['values']

    # This method is responsible for returning the continent id and continent name
    def get_continent_values(self, continent):
        return continent.values('id','name')

# The Country Serializer class will inherit the ModelSerializer class from the serializers package
class CountrySerializer(serializers.ModelSerializer):
    # The JSON object to returned will be labeled values, that will get its data from a class method called get_country_values
    values = serializers.SerializerMethodField('get_country_values')

    class Meta:
        # We will set the serializer class to get data from the Country model
        model = Country
        # We then set the class to return values variable as the desired response
        fields = ['values']

    # This method is responsible for returning the country id, country name and country code
    def get_country_values(self, country):
        return country.values('id','name','code')

# The Cities Serializer class will inherit the ModelSerializer class from the serializers package
class CitiesSerializer(serializers.ModelSerializer):
    # The JSON object to returned will be labeled values, that will get its data from a class method called get_cities_values
    values = serializers.SerializerMethodField('get_cities_values')

    class Meta:
        # We will set the serializer class to get data from the Cities model
        model = Cities
        # We then set the class to return values variable as the desired response
        fields = ['values']

    # This method is responsible for returning the cities name
    def get_cities_values(self, cities):
        return cities.values('name')

# The A.P.I. Serializer class will inherit the ModelSerializer class from the serializers package
class APISerializer(serializers.ModelSerializer):
    class Meta:
        # We will set the serializer class to get data from the A.P.I. model
        model = API
        # We then set the class to return the A.P.I. name and A.P.I. code as the desired response
        fields = ['id','name','code']

# The Subscription Serializer class will inherit the ModelSerializer class from the serializers package
class SubscriptionSerializer(serializers.ModelSerializer):
    # The JSON object to returned will be labeled values, that will get its data from a class method called get_subscription_values
    values = serializers.SerializerMethodField('get_subscription_values')

    class Meta:
        # We will set the serializer class to get data from the Subscription model
        model = Subscription
        # We then set the class to return values variable as the desired response
        fields = ['values']

    # This method is responsible for returning the subscribed A.P.I. id and A.P.I. name
    def get_subscription_values(self, subscription):
        return subscription.values('api__id','api__name')